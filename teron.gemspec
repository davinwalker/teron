Gem::Specification.new do |spec|
  spec.name          = 'teron'
  spec.version       = '0.0.8'
  spec.authors       = ['Davin Walker']
  spec.email         = ['dishcandanty@gmail.com']

  spec.summary       = 'Experimental Simple Class Initialization'
  spec.description   = 'Gimme an easy way to initialize database less classes'
  spec.homepage      = 'https://gitlab.com/davinwalker/teron'
  spec.license       = 'MIT'

  # Specify which files should be added to the gem when it is released.
  spec.files = Dir['lib/**/*']
  spec.require_paths = ['lib']
  spec.add_dependency 'activesupport', '>= 6.0'
  spec.add_development_dependency 'pry', '~> 0.12'
  spec.add_development_dependency 'rubocop', '~> 0.80'
end
