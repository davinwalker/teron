# Saving/Records Keeper
module TeronRecords
  def save!
    self.class._mutex.synchronize do
      # Handle Dump vs No Dump Variables
      payload = self.class.class_variable_get(:@@params).each_with_object({}) do |data, obj|
        value = attributes[data[:name]]
        obj[data[:name]] = data.dig(:options, :dump) ? Marshal.load(Marshal.dump(value)) : value

        obj
      end

      self.class._data[id] = payload
    end

    self.class._obj_clear id

    true
  end

  alias save save!

  def destroy!
    self.class._mutex.synchronize do
      self.class._data.delete id
    end

    self.class._obj_clear id

    true
  end

  def attributes
    instance_variables.each_with_object({}) do |name, obj|
      key = name.to_s.delete('@').to_sym
      obj[key] = instance_variable_get(name)

      obj
    end
  end

  def reload
    klass_obj = self.class.find id

    instance_variables.each do |name|
      key = name.to_s.delete('@').to_sym
      instance_variable_set(name, klass_obj.send(key))
    end

    true
  end
end
