# ==============================================================
#          ___ ____ ____ ____ __ _
#          |  |=== |--< [__] | \|
#
#  ======== [ The Devious Teron ] ========
# ==============================================================

# Attributes
require 'securerandom'
require_relative 'field'

# In Memory Collection
require_relative 'record'

# Associations
require 'active_support/core_ext/string/inflections' ## Constantize

require_relative 'associations/has_many'
require_relative 'associations/has_one'
require_relative 'associations/belongs_to'

# Initializtion Helper
class Teron
  attr_accessor :id

  include TeronRecords
  extend TeronField
  extend TeronAssociation

  def inspect
    "#<#{self.class.name} #{inspect_vars.join(', ')}>"
  end

  def inspect_vars
    instance_variables.map do |name|
      "#{name}: #{instance_variable_get(name)}"
    end
  end

  def initialize(params = {})
    self.class.class_variable_get(:@@params).each do |param|
      value = if params.key? param[:name]
                params[param[:name]]
              else
                default = param.dig(:options, :default)
                if default.instance_of?(Proc)
                  default.call
                else
                  # Ensure Deep Clone
                  param.dig(:options, :dump) ? Marshal.load(Marshal.dump(default)) : default

                end
              end

      instance_variable_set("@#{param[:name]}", value) if value
    end
  end

  class << self
    def _data
      class_variable_get(:@@data)
    end

    # Store of In Memory already loaded objects
    def _object_list
      class_variable_get(:@@object_list)
    end

    def _mutex
      class_variable_get(:@@mutex)
    end

    def first
      return nil if count.zero?

      _obj_create _data.values[0]
    end

    def last
      return nil if count.zero?

      _obj_create _data.values[-1]
    end

    def count
      _data.size
    end

    def _obj_create(obj_data)
      _mutex.synchronize do
        _object_list[obj_data[:id]] = new obj_data unless _object_list.key? obj_data[:id]
      end

      _object_list[obj_data[:id]]
    end

    def _obj_clear(obj_id)
      _mutex.synchronize do
        _object_list.delete obj_id if _object_list.key? obj_id
      end
    end

    def find_by(args)
      return nil if args.nil?
      return nil if count.zero?

      key, value = args.first
      obj = _data.values.find { |x| x[key] == value }

      return nil unless obj

      _obj_create obj
    end

    def where(args)
      return [] if args.nil?
      return [] if count.zero?

      objs = _data.values.select do |value|
        args.all? { |k, v| value[k] == v }
      end

      objs.map { |x| _obj_create x }
    end

    def all
      return [] if count.zero?

      _data.values.map { |x| _obj_create x }
    end

    def find(key)
      return nil if key.nil?
      return nil if count.zero?

      return nil unless _data.key? key

      _obj_create _data[key]
    end
  end
end
