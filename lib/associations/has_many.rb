# Associations for Has Many
module TeronAssociation
  # The Object to do the including
  def has_many(class_name, param_options = {})
    # Should Inverse Object Be Updated
    inverse = param_options[:inverse] || true

    field "#{class_name}_ids".to_sym, default: -> { [] }

    # ==================================
    # Accessor
    # ==================================
    define_method(class_name) do
      # Get Constant
      klass = class_name.to_s.capitalize.singularize.classify.constantize

      # Get Entry Ids
      klass_list = send("#{class_name}_ids".to_sym)
      return klass_list if klass_list.empty?

      klass_list.map { |x| klass.find x }.compact
    end

    # ==================================
    # Add
    # ==================================
    define_method("#{class_name}_add") do |klass_obj|
      # Get Constant
      klass = class_name.to_s.capitalize.singularize.classify.constantize

      raise "Invalid #{klass}; Received #{klass_obj.class})" unless klass_obj.instance_of?(klass)

      klass_list = send("#{class_name}_ids".to_sym)
      klass_list.push klass_obj.id unless klass_list.include? klass_obj.id

      # Klass Many Name
      klass_many_name = self.class.to_s.underscore

      # Update Obj Association
      if inverse
        klass_obj.send("#{klass_many_name}=", self)
        klass_obj.save!
      end

      save!
    end

    # ==================================
    # Remove (Meta)
    # Don't think this whould be called directly
    # ==================================
    define_method("#{class_name}_remove") do |klass_obj|
      # Get Constant
      klass = class_name.to_s.capitalize.singularize.classify.constantize

      raise "Invalid #{klass}; Received #{klass_obj.class})" unless klass_obj.instance_of?(klass)

      klass_list = send("#{class_name}_ids".to_sym)
      klass_list.delete klass_obj.id if klass_list.include? klass_obj.id

      # Remove Association on removed object
      if inverse
        klass_many_name = self.class.to_s.underscore
        klass_obj.send("#{klass_many_name}_id=", nil)
      end

      save!
    end

    # ==================================
    # Create
    # ==================================
    define_method("#{class_name}_create") do |opts = {}|
      # Get Constant
      klass = class_name.to_s.capitalize.singularize.classify.constantize
      klass_obj = klass.new(opts)

      send("#{class_name}_add", klass_obj)

      klass_obj
    end

    # ==================================
    # Destroy All
    # ==================================
    define_method("#{class_name}_destroy_all") do
      send(class_name).each(&:destroy!) if inverse
    end
  end
end
