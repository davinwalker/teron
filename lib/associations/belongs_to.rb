# Associations for Has Many
module TeronAssociation
  # The Object to be included
  def belongs_to(class_name, param_options = {})
    # Should Inverse Object Be Updated
    inverse = param_options[:inverse] || true

    field "#{class_name}_id".to_sym

    # ==================================
    # Accessor
    # ==================================
    define_method(class_name) do
      # Get Constant
      klass = class_name.to_s.capitalize.singularize.classify.constantize

      # Get Entry Id
      klass_obj_id = send("#{class_name}_id".to_sym)
      klass.find klass_obj_id
    end

    # ==================================
    # Add
    # ==================================
    define_method("#{class_name}=") do |klass_obj|
      # Get Constant
      klass = class_name.to_s.capitalize.singularize.classify.constantize

      raise "Invalid #{klass}; Received #{klass_obj.class})" unless klass_obj.instance_of?(klass)

      send("#{class_name}_id=", klass_obj.id)
      save!
    end

    # ==================================
    # Destroy
    # ==================================
    define_method('destroy!') do
      # Get Association Name
      klass_name = self.class.name.downcase.singularize
      # self.class._obj_clear(id)

      # Remove from included object`
      send(class_name)&.send("#{klass_name}_remove", self) if inverse

      super()
    end
  end
end
